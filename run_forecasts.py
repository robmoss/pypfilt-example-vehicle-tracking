#!/usr/bin/env python

import pypfilt

config_file = 'scenario.toml'
output_file = 'example.hdf5'
forecast_times = [1.0, 2.0, 3.0, 4.0, 5.0]

for forecast in pypfilt.forecasts_iter(config_file):
    pypfilt.forecast(forecast.params, forecast.observation_streams,
                     forecast_times, filename=output_file)
