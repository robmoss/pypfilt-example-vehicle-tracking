# Example of vehicle tracking using pypfilt

Create a Python virtual environment and install the required packages:

    pip install -r requirements.txt

Then run the forecasts for the observations in `data/example-observations.ssv`:

    ./run_forecasts.py

This will save the results to `forecasts/example.hdf5`.

If you have `R` installed you can plot the forecast credible intervals for each state variable:

    ./plot_forecasts.R

This will save the plot to `forecasts.png`.

![Forecast plot](forecasts.png)
