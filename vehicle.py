"""
A minimal example of vehicle tracking using pypfilt.
"""

import pypfilt
import scipy.stats


class Vehicle(pypfilt.Model):
    """
    A vehicle model that has a current location and a fixed velocity.

    This model has four state variables:

    - `lat`: the current latitude;
    - `lon`: the current longitude;
    - `speed_lat`: the speed of movement with respect to latitude; and
    - `speed_lon`: the speed of movement with respect to longitude.
    """

    def init(self, ctx, vec):
        # Retrieve the random number generator for the simulation model.
        rnd = ctx.component['random']['model']
        # Determine how many particles must be defined.
        size = vec.shape[0]
        # Sample the starting location from the prior distribution.
        vec[..., 0] = ctx.params['model']['prior']['lat'](rnd, size)
        vec[..., 1] = ctx.params['model']['prior']['lon'](rnd, size)
        # Sample the fixed velocity from the prior distribution.
        vec[..., 2] = ctx.params['model']['prior']['speed_lat'](rnd, size)
        vec[..., 3] = ctx.params['model']['prior']['speed_lon'](rnd, size)

    def state_size(self):
        # There are four elements: lat, lon, speed_lat, speed_lon.
        return 4

    def describe(self):
        return [
            ('lat', True, 0, 90),
            ('lon', True, 0, 180),
            ('speed_lat', True, -0.001, 0.001),
            ('speed_lon', True, -0.001, 0.001),
        ]

    def update(self, ctx, time, dt, is_fs, prev, curr):
        # Maintain the current velocity.
        curr[..., 2] = prev[..., 2]
        curr[..., 3] = prev[..., 3]
        # Update the vehicle position.
        curr[..., 0] = prev[..., 0] + dt * curr[..., 2]
        curr[..., 1] = prev[..., 1] + dt * curr[..., 2]


class LatLon(pypfilt.Obs):
    """
    An observation model for latitude and longitude.
    """

    def __init__(self, obs_unit):
        self.unit = obs_unit
        self.period = 0

    def log_llhd(self, ctx, op, time, obs, curr, hist):
        obs_sdev = op['sdev']

        px_lat = curr[..., 0]
        px_lon = curr[..., 1]
        dist_lat = scipy.stats.norm(loc=px_lat, scale=obs_sdev)
        dist_lon = scipy.stats.norm(loc=px_lon, scale=obs_sdev)

        # Calculate the likelihood of the observed latitude and longitude,
        # according to the current vehicle position for each particle.
        obs_lat = obs['latitude']
        obs_lon = obs['longitude']
        llhd_lat = dist_lat.logpdf(obs_lat)
        llhd_lon = dist_lon.logpdf(obs_lon)

        # Return the combined log-likelihood of the observed latitude and
        # longitude for each particle.
        return llhd_lat + llhd_lon

    def expect(self, ctx, op, time, period, prev, curr):
        # The expected observation is the current vehicle position, under the
        # assumption that the observations are unbiased (mean error of zero).
        px_lat = curr[..., 0]
        px_lon = curr[..., 1]
        size = curr.shape[0]
        return [(px_lat[i], px_lon[i]) for i in range(size)]

    def quantiles(self, ctx, op, time, mu, wt, probs):
        raise NotImplementedError()

    def simulate(self, ctx, op, time, period, expected, rng=None):
        raise NotImplementedError()

    def from_file(self, filename, time_scale):
        columns = [('time', float), ('latitude', float), ('longitude', float)]
        data = pypfilt.io.read_table(filename, columns)
        obs_list = [
            {'date': data['time'][i],
             'latitude': data['latitude'][i],
             'longitude': data['longitude'][i],
             'unit': self.unit,
             'period': self.period,
             }
            for i in range(len(data))]

        return (obs_list, data)
